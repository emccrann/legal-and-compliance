## Statutory Leave Policy Audit

This issue should be completed monthly to track updates to statutory leave entitlements in our entity locations.  Each checkbox should be marked to confirm that statutory entitlements (annual leave, parental leave, sick leave, and any other statutory leaves) have been reviewed. If changes to statutory leave entitlements need to be reflected in the handbook, tag the absence management team to create an MR.

### Step 1 - Name Issue 

- [ ] Name the issue: _Statutory Leave Audit YYYY-MM-DD_

### Step 2 - Legal - Review Statutory Entitlements 
- [ ] Research updates to statutory leave (using Bloomberg Law, Compliance HR, Littler Global Guide, etc.) and make note of any updates under the respective entity location.

- [ ] Check the checkbox to confirm that statutory entitlements found on [entity-specific benefits pages](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#entity-benefits) have been reviewed and/or updates have been noted.

- [ ] United States
- [ ] Belgium
- [ ] Finland
- [ ] Netherlands
- [ ] Germany
- [ ] Australia
- [ ] New Zealand
- [ ] United Kingdom
- [ ] France
- [ ] Canada
- [ ] Japan
- [ ] Ireland
- [ ] Singapore
- [ ] South Korea

### Step 3 - Update Handbook
**For Absence Management**
- [ ] Create MRs to update handbook or check N/A if there are no changes
    - [ ] N/A 
- [ ] Link all MRs to this issue.
- [ ] Tag @legal-employment to review the updates.

<!--Do not edit below this line-->

/confidential

/label ~Employment 
