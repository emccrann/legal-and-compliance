This template will be used to document audit results of the quarterly acceptable use audit of Jamf. 

**Date of Audit:**
**Auditor:**

---
---
### Results of Audit

- [ ] No misuse was found.
- [ ] Misuse was found.

---
---
### Documentation 

*insert screenshot of users with Admin access*
*link to related Legal issue in "Related Issues" section below.*

------

/confidential

/label ~"Legal Compliance"
