<!-- Refer to the single material legal review process (about.gitlab.com/handbook/legal/materials-legal-review-process/#track-1-single-material-legal-review-process) and complete this template to obtain legal review of a single piece of material. If you need to obtain legal review of multiple pieces of material with a related purpose, like several slide decks being prepared for one event, complete the multiple materials legal review issue template instead. -->

<!-- Title the issue as follows: “[name of material]_Deck_Materials Legal Review”, for example “All Remote Evangelism Slide Deck_Materials Legal Review”-->

### Materials to be reviewed
<!-- Link (for Google Docs) or upload (for other file types) the material for review here. --> 


### Is this material for internal or external use?
<!-- Delete as appropriate, and refer to the definitions of `external use` and `internal use` in the Materials Legal Review Process. If there are plans to use the material, or any part of it, externally in the future, chose `external`. -->
- external [@hloeffert @LeeFalc]
- internal [@hloeffert]

### Will the materials be made available on GitLab Unfiltered, Edcast, or anywhere else?
<!-- Delete as appropriate to state whether some or all of the materials being submitted for review will be made available anywhere. If they will, give details of the visibility the materials will have. -->
- yes <!-- if yes, give details -->
- no

### Due date for review
<!-- State the due date for review, and indicate this as the due date of the issue below, noting that the Legal and Corporate Affairs Team requires at least two business days to complete a review. -->
----
<!-- Do Not Edit Below -->
/confidential
